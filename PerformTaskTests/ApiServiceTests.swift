//
//  ApiServiceTests.swift
//  PerformTaskTests
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import XCTest
import Alamofire

@testable import PerformTask

class ApiServiceTests: XCTestCase {
    
    var apiService: ApiServiceProtocol?
    
    override func setUp() {
        super.setUp()
        apiService = ApiService()
    }
    
    override func tearDown() {
        super.tearDown()
        apiService = nil
    }
    
    func testGetScores() {
        let exp = self.expectation(description: "testGetScores expectation")
        apiService?.fetchObject(from: .scores, callback: { (response: DataResponse<Score>) in
            switch response.result {
            case .success(let scores):
                XCTAssertNotNil(scores, "Scores cannot be nil")
                XCTAssertNotEqual(scores.matches.count, 0, "Matches amount cannot be zero")
            case .failure(let error):
                XCTAssertNil(error, "Error should be nil")
            }
            exp.fulfill()
        })
        waitForExpectations(timeout: 15) { (error) in
            XCTAssertNil(error, error?.localizedDescription ?? "")
        }
    }
    
    func testGetChannels() {
        let exp = self.expectation(description: "testGetChannels expectation")
        apiService?.fetchCollection(from: .news, callback: { (response: DataResponse<[Channel]>) in
            switch response.result {
            case .success(let channels):
                XCTAssertNotNil(channels, "Channels cannot be nil")
                XCTAssertNotEqual(channels.count, 0, "Channel amount cannot be zero")
                XCTAssertNotEqual(channels.first?.items.count, 0, "Channel items amount cannot be zero")
            case .failure(let error):
                XCTAssertNil(error, "Error should be nil")
            }
            exp.fulfill()
        })
        waitForExpectations(timeout: 15) { (error) in
            XCTAssertNil(error, error?.localizedDescription ?? "")
        }
    }
    
    func testGetStandings() {
        let exp = self.expectation(description: "testGetStandings expectation")
        apiService?.fetchObject(from: .standing, callback: { (response: DataResponse<Standing>) in
            switch response.result {
            case .success(let standings):
                XCTAssertNotNil(standings, "Standings cannot be nil")
                XCTAssertNotEqual(standings.rankings.count, 0, "Standings rankings amount cannot be zero")
            case .failure(let error):
                XCTAssertNil(error, "Error should be nil")
            }
            exp.fulfill()
        })
        waitForExpectations(timeout: 15) { (error) in
            XCTAssertNil(error, error?.localizedDescription ?? "")
        }
    }
}

