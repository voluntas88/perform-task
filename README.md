# Perform Test #

### How to run the project? ###

* navigate to project dorectory in Terminal and run 'pod install'
* open PerformTask.xcworkspace and run the project

### Notes ###

* Swift 4
* MVVM pattern
* Dependency Injection
* BTNavigationDropdownMenu library is downloading my from fork, which allows to add UIBarButtonItem as toggle button for menu show/hide action
* Unit Tests are wrritten for ApiService

### Used libraries ###

* [Alamofire](https://github.com/Alamofire/Alamofire) - networking
* [AlamofireImage](https://github.com/Alamofire/AlamofireImage) -  Image with URL
* [BTNavigationDropdownMenu](https://github.com/vlado-rudenok/BTNavigationDropdownMenu) - for drop down menu
* [SWXMLHash](https://github.com/drmohundro/SWXMLHash) - safely XML parsing
* [Swinject](https://github.com/Swinject/Swinject), [SwinjectStoryboard](https://github.com/Swinject/SwinjectStoryboard) - dependency injection
* [RxSwift](https://github.com/ReactiveX/RxSwift) - Reactive Programming
* [RxDataSources](https://github.com/RxSwiftCommunity/RxDataSources) - UTableView data sources for RXSwift
* [PKHUD](https://github.com/pkluz/PKHUD) - progress hud
* [UIColor_Hex_Swift](https://github.com/yeahdongcn/UIColor-Hex-Swift) - UIColor from hex string

### Who do I talk to? ###

* Repo owner
