//
//  APIService.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 18/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import Alamofire

internal final class ApiService: ApiServiceProtocol {
    
    // MARK: - API fetching
    func fetchObject<T: XMLSerializable>(from endpoint: ApiEndpoint, callback: @escaping (DataResponse<T>) -> ()) {
        let _ = endpoint.request.validate().responseObjectForKeyPath(keyPath: endpoint.responseNodePath) { (response) in
            callback(response)
        }
    }
    
    func fetchCollection<T: XMLSerializable>(from endpoint: ApiEndpoint, callback: @escaping (DataResponse<[T]>) -> ()) {
        let _ = endpoint.request.validate().responseArrayForKeyPath(keyPath: endpoint.responseNodePath) { (response) in
            callback(response)
        }
    }
}
