//
//  APIService+Alamofire.swift
//
//  Created by Maciek on 07.10.2015.
//  Copyright © 2015 Code Latte. All rights reserved.
//

import SWXMLHash
import Alamofire


enum ModelError: Error {
    case BadXML(XMLIndexer)
}

public protocol XMLSerializable {
    init(node: XMLIndexer) throws
}

extension Alamofire.DataRequest {
    public func responseObjectForKeyPath<T: XMLSerializable>(keyPath: String, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        let responseSerializer = DataResponseSerializer<T> { request, response, data, error in
            guard let data = data else {
                return .failure(AFError.responseValidationFailed(reason: .dataFileNil))
            }
            
            do {
                var xml = SWXMLHash.parse(data)

                for key in keyPath.components(separatedBy: ".") {
                    xml = try xml.byKey(key)
                }
                let model = try T(node: xml)
                return .success(model)
            } catch let error {
                return .failure(error)
            }
        }
        
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
    
    public func responseArrayForKeyPath<T: XMLSerializable>(keyPath: String, completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self {
        let responseSerializer = DataResponseSerializer<[T]> { request, response, data, error in
            guard let data = data else {
                return .failure(AFError.responseValidationFailed(reason: .dataFileNil))
            }
            
            do {
                var xml = SWXMLHash.parse(data)
                
                for key in keyPath.components(separatedBy: ".") {
                    xml = try xml.byKey(key)
                }
                
                let models = try xml.all.map { try T(node: $0) }
                
                return .success(models)
            } catch let error {
                return .failure(error)
            }
        }
        
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
}
