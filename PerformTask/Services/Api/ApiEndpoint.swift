//
//  ApiEndpoints.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 18/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import Alamofire

internal enum ApiEndpoint {
    case news
    case scores
    case standing
}

internal extension ApiEndpoint {
    var request: Alamofire.DataRequest {
        switch self {
        case .news: return Alamofire.request(url, method: .get)
        case .scores: return Alamofire.request(url, method: .get)
        case .standing: return Alamofire.request(url, method: .get)
        }
    }
    
    var responseNodePath: String {
        switch self {
        case .news: return "rss.channel"
        case .scores: return "gsmrs"
        case .standing: return "gsmrs.competition"
        }
    }
}

fileprivate extension ApiEndpoint {
    fileprivate var url: String {
        return "http://www.mobilefeeds.performgroup.com/utilities/interviews/techtest\(relativeURLString)"
    }
    
    private var relativeURLString: String {
        switch self {
        case .news: return "/latestnews.xml"
        case .scores: return "/scores.xml"
        case .standing: return "/standings.xml"
        }
    }
}
