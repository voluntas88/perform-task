//
//  ApiServiceProtocol.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import Alamofire

internal protocol ApiServiceProtocol: class {
    func fetchObject<T: XMLSerializable>(from endpoint: ApiEndpoint, callback: @escaping (DataResponse<T>) -> ())
    func fetchCollection<T: XMLSerializable>(from endpoint: ApiEndpoint, callback: @escaping (DataResponse<[T]>) -> ())
}
