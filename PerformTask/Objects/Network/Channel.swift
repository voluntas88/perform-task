//
//  Channel.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 18/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import SWXMLHash

internal struct Channel {
    var title: String = ""
    var items: [ChannelItem] = []
}

extension Channel: XMLSerializable {
    init(node: XMLIndexer) throws {
        guard let title = node["title"].element?.text
            else {
                throw ModelError.BadXML(node)
        }
        self.title = title
        
        do {
            self.items = try node["item"].all.map { try ChannelItem(node: $0) }
        } catch let e {
            print(e.localizedDescription)
        }
    }
}
