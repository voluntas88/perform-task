//
//  Standing.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 18/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import SWXMLHash

internal struct Standing {
    var title: String = ""
    var rankings: [Ranking] = []
}

extension Standing: XMLSerializable {
    init(node: XMLIndexer) throws {
        guard let title = node.element?.attribute(by: "name")?.text
            else {
                throw ModelError.BadXML(node)
        }
        self.title = title
        
        do {
            self.rankings = try node["season"]["round"]["resultstable"]["ranking"].all.map { try Ranking(node: $0) }
        } catch let e {
            print(e.localizedDescription)
        }
    }
}
