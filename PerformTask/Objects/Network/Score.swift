//
//  Competition.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 18/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import SWXMLHash

internal struct Score {
    var title: String = ""
    var matches: [Match] = []
}

extension Score: XMLSerializable {
    init(node: XMLIndexer) throws {
        guard let title = node["method"]["parameter"][0].element?.attribute(by: "value")?.text
            else {
                throw ModelError.BadXML(node)
        }
        self.title = title
        
        do {
            let groups = node["competition"]["season"]["round"]["group"].all
            for group in groups {
                let m = try group["match"].all.map { try Match(node: $0) }
                self.matches.append(contentsOf: m)
            }
        } catch let e {
            print(e.localizedDescription)
        }
    }
}
