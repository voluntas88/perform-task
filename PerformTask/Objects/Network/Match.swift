//
//  Match.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 18/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import SWXMLHash

internal struct Match {
    var matchId: String
    var teamA: Team
    var teamB: Team
    var status: String
    var lastUpdated: String
}

extension Match: XMLSerializable{
    init(node: XMLIndexer) throws {
        guard let matchId = node.element?.attribute(by: "match_id")?.text,
            let teamAId = node.element?.attribute(by: "team_A_id")?.text,
            let teamAName = node.element?.attribute(by: "team_A_name")?.text,
            let teamACountry = node.element?.attribute(by: "team_A_country")?.text,
            let teamBId = node.element?.attribute(by: "team_B_id")?.text,
            let teamBName = node.element?.attribute(by: "team_B_name")?.text,
            let teamBCountry = node.element?.attribute(by: "team_B_country")?.text,
            let status = node.element?.attribute(by: "status")?.text,
            let fsA = node.element?.attribute(by: "fs_A")?.text,
            let fsB = node.element?.attribute(by: "fs_B")?.text,
            let lastUpdated = node.element?.attribute(by: "last_updated")?.text
            else {
                throw ModelError.BadXML(node)
        }
        self.matchId = matchId
        self.teamA = Team(id: teamAId, name: teamAName, country: teamACountry, fs: fsA)
        self.teamB = Team(id: teamBId, name: teamBName, country: teamBCountry, fs: fsB)
        self.status = status
        self.lastUpdated = lastUpdated
    }
}

extension Match {
    var teamAName: String {
        return "\(teamA.name) (\(teamA.country))"
    }
    
    var teamsScore: String {
        return "\(teamA.fs) - \(teamB.fs)"
    }
    
    var teamBName: String {
        return "\(teamB.name) (\(teamB.country))"
    }
}
