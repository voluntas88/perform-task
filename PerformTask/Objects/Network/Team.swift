//
//  Team.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 18/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation

internal struct Team {
    var id: String
    var name: String
    var country: String
    var fs: String
}
