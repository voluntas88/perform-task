//
//  ChannelItem.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 18/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import SWXMLHash

internal struct ChannelItem {
    var guid: String
    var title: String
    var pubDate: String
    var imageUrl: String
    var description: String?
    var link: String
}

extension ChannelItem: XMLSerializable {
    init(node: XMLIndexer) throws {
        guard let guid = node["guid"].element?.text,
              let title = node["title"].element?.text,
              let pubDate = node["pubDate"].element?.text,
              let imageUrl = node["enclosure"].element?.attribute(by: "url")?.text,
              let link = node["link"].element?.text
            else {
            throw ModelError.BadXML(node)
        }
        self.guid = guid
        self.title = title
        self.pubDate = pubDate
        self.imageUrl = imageUrl
        self.description = node["description"].element?.text
        self.link = link
    }
}
