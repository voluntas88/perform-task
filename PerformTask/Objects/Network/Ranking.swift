//
//  Ranking.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 18/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import SWXMLHash

internal struct Ranking {
    var rank: Int
    var lastRank: Int
    var zoneStart: String?
    var teamId: String
    var clubName: String
    var countrycode: String?
    var areaId: String?
    var matchesTotal: String
    var matchesWon: String
    var matchesDraw: String
    var matcheLost: String
    var goalsPro: Int
    var goalsAgainst: Int
    var points: String
}

extension Ranking: XMLSerializable {
    init(node: XMLIndexer) throws {
        guard let rank = node.element?.attribute(by: "rank")?.text,
            let lastRank = node.element?.attribute(by: "last_rank")?.text,
            let teamId = node.element?.attribute(by: "team_id")?.text,
            let clubName = node.element?.attribute(by: "club_name")?.text,
            let matchesTotal = node.element?.attribute(by: "matches_total")?.text,
            let matchesWon = node.element?.attribute(by: "matches_won")?.text,
            let matchesDraw = node.element?.attribute(by: "matches_draw")?.text,
            let matcheLost = node.element?.attribute(by: "matches_lost")?.text,
            let goalsPro = node.element?.attribute(by: "goals_pro")?.text,
            let goalsAgainst = node.element?.attribute(by: "goals_against")?.text,
            let points = node.element?.attribute(by: "points")?.text,
            let rankInt = Int(rank),
            let lastRankInt = Int(lastRank),
            let goalsProInt = Int(goalsPro),
            let goalsAgainstInt = Int(goalsAgainst)
            else {
                throw ModelError.BadXML(node)
        }
        self.rank = rankInt
        self.lastRank = lastRankInt
        self.zoneStart = node.element?.attribute(by: "zone_start")?.text
        self.teamId = teamId
        self.clubName = clubName
        self.countrycode = node.element?.attribute(by: "countrycode")?.text
        self.areaId = node.element?.attribute(by: "area_id")?.text
        self.matchesTotal = matchesTotal
        self.matchesWon = matchesWon
        self.matchesDraw = matchesDraw
        self.matcheLost = matcheLost
        self.goalsPro = goalsProInt
        self.goalsAgainst = goalsAgainstInt
        self.points = points
    }
}
