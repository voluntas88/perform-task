//
//  RootViewModel.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation

internal struct RootViewModel {
    let menuItems = [Localizable.menuItemNews.localized, Localizable.menuItemScores.localized, Localizable.menuItemStanding.localized]
}
