//
//  RootViewController.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu

internal final class RootViewController: BaseViewController {
    
    var viewModel: RootViewModel!
    
    fileprivate let indexes = (news: 0, scores: 1, standings: 2)
    
    @IBOutlet weak var newsContainerView: UIView!
    @IBOutlet weak var scoresContainerView: UIView!
    @IBOutlet weak var standingsContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initMenu()
        showContainerView(for: indexes.news)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
fileprivate extension RootViewController {
    func initMenu() {
        let toggleButton = UIBarButtonItem(image: UIImage(named: "menu"), style: .plain, target: nil, action: nil)
        let menuView = BTNavigationDropdownMenu(navigationItem: navigationItem, toggleButton: toggleButton, navigationController: navigationController, containerView: navigationController!.view, title: BTTitle.index(0), items: viewModel.menuItems)
        self.navigationItem.titleView = menuView
        menuView.arrowTintColor = UIColor.clear
        menuView.didSelectItemAtIndexHandler = {[weak self] (index: Int) -> () in
            self?.showContainerView(for: index)
        }
    }

    
    func showContainerView(for index: Int) {
        UIView.animate(withDuration: 0.3, animations: {
            self.newsContainerView.alpha = index != self.indexes.news ? 0 : 1
            self.scoresContainerView.alpha = index != self.indexes.scores ? 0 : 1
            self.standingsContainerView.alpha = index != self.indexes.standings ? 0 : 1
        }) { (_) in
            self.newsContainerView.isHidden = index != self.indexes.news
            self.scoresContainerView.isHidden = index != self.indexes.scores
            self.standingsContainerView.isHidden = index != self.indexes.standings
        }
    }
}
