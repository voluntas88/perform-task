//
//  RootAssembly.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

internal final class RootAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(RootViewModel.self) { (r) in
            RootViewModel()
        }
        
        container.storyboardInitCompleted(RootViewController.self) { r, viewController in
            let viewModel = r.resolve(RootViewModel.self)
            viewController.viewModel = viewModel
        }
    }
}
