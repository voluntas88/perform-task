//
//  NewsContentViewModel.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation

internal final class NewsContentViewModel {
    var contentUrl: URL?
    
    var noContentHtml: String {
        return "<html><head></head><body><h1>\(noContentLocalizable)</h1></body></html>"
    }
}

extension NewsContentViewModel {
    fileprivate var noContentLocalizable: String {
        return Localizable.urlNoContentUrl.localized
    }
}
