//
//  NewsContentViewController.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import UIKit
import WebKit
import PKHUD

class NewsContentViewController: BaseViewController {

    var viewModel: NewsContentViewModel!
    
    fileprivate var contentWebView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        initContentWebView()
        loadContentUrl()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setContentLink(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        viewModel.contentUrl = url
    }
}

extension NewsContentViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideProgressHud()
    }
}

fileprivate extension NewsContentViewController {
    
    func initContentWebView() {
        contentWebView = WKWebView()
        contentWebView.frame = view.bounds
        contentWebView.navigationDelegate = self
        contentWebView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        view.addSubview(contentWebView)
    }
    
    func loadContentUrl() {
        if let url = viewModel.contentUrl {
            contentWebView.load(URLRequest(url: url))
        } else {
            contentWebView.loadHTMLString(viewModel.noContentHtml, baseURL: nil)
        }
        
        showProgressHud()
    }
}
