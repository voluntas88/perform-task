//
//  NewsContentAssembly.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

internal final class NewsContentAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(NewsContentViewModel.self) { (r) in
            return NewsContentViewModel()
        }
        
        container.storyboardInitCompleted(NewsContentViewController.self) { r, viewController in
            let viewModel = r.resolve(NewsContentViewModel.self)
            viewController.viewModel = viewModel
        }
    }
}

extension NewsContentAssembly {
    static var viewController: NewsContentViewController  {
        return storyboard.instantiateViewController(withIdentifier: viewControllerIdentifier) as! NewsContentViewController
    }
    
    static var storyboard: SwinjectStoryboard {
        return SwinjectStoryboard.create(name: "Main", bundle: nil)
    }
    
    static var viewControllerIdentifier: String {
        return "NewsContentViewController"
    }
}
