//
//  ScoresItemViewModel.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import RxDataSources

internal class MatchViewModel {
    var matchId: String
    var leftText: String
    var rightText: String
    var centerText: String
    
    init(match: Match) {
        self.matchId = match.matchId
        self.leftText = match.teamAName
        self.rightText = match.teamBName
        self.centerText = match.teamsScore
    }
}

//MARK: - Equatable
extension MatchViewModel: Equatable {
    static func ==(lhs: MatchViewModel, rhs: MatchViewModel) -> Bool {
        return lhs.matchId == rhs.matchId
    }
}

//MARK: - IdentifiableType
extension MatchViewModel: IdentifiableType {
    typealias Identity = String
    
    var identity: String {
        return matchId
    }
}

extension MatchViewModel {
    var cellIdentifier: String {
        return "match_cell"
    }
}
