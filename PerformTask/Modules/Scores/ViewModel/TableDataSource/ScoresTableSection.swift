//
//  ScoresTableSection.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import RxDataSources

internal struct ScoresTableSection {
    var title: String
    var items: [MatchViewModel]
    
    init(title: String, items: [MatchViewModel]) {
        self.title = title
        self.items = items
    }
}

//MARK: - AnimatableSectionModelType
extension ScoresTableSection: AnimatableSectionModelType {
    
    typealias Identity = String
    
    var identity: Identity {
        return title
    }
    
    init(original: ScoresTableSection, items: [MatchViewModel]) {
        self = original
        self.items = items
    }
}
