//
//  ScoresViewModel.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

internal class ScoresViewModel {
    fileprivate var apiService: ApiServiceProtocol!
    
    fileprivate(set) var tableViewSections: Variable<[ScoresTableSection]> = Variable([])
    fileprivate(set) var scores: Variable<Score> = Variable(Score())
    
    fileprivate let disposeBag = DisposeBag()
    
    fileprivate var autoRefreshTime: Timer?
    
    init(apiService: ApiServiceProtocol?) {
        self.apiService = apiService
        self.bind()
    }
}

internal extension ScoresViewModel {
    func bind() {
        scores.asObservable().map({ (scoresInner) -> [ScoresTableSection] in
            let scoresItemsViewModels = scoresInner.matches.map { MatchViewModel(match: $0) }
            let section = ScoresTableSection(title: scoresInner.title, items: scoresItemsViewModels)
            return [section]
        }).bind(to: tableViewSections)
            .disposed(by: disposeBag)
    }
    
    func getScores() {
        apiService.fetchObject(from: .scores) { (response: DataResponse<Score>) in
            switch response.result {
            case .success(let scores):
                self.scores.value = scores
            case .failure(_):
                break
            }
            self.sheduleAutoRefreshTimer()
        }
    }
}

fileprivate extension ScoresViewModel {
    func sheduleAutoRefreshTimer()
    {
        autoRefreshTime = Timer.scheduledTimer(withTimeInterval: 30, repeats: true, block: { (_) in
            self.getScores()
        })
    }
}

