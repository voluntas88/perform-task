//
//  MatchTableViewCell.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import UIKit

class MatchTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var leftTextLabel: UILabel!
    @IBOutlet fileprivate weak var rightTextLabel: UILabel!
    @IBOutlet fileprivate weak var centerTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func populate(with viewModel:MatchViewModel) {
        leftTextLabel.text = viewModel.leftText
        rightTextLabel.text = viewModel.rightText
        centerTextLabel.text = viewModel.centerText
    }
    
}
