//
//  ScoresViewController.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

class ScoresViewController: BaseViewController {

    var viewModel: ScoresViewModel!
    
    fileprivate let rowHeight:CGFloat = 32.0
    
    fileprivate var dataSource: RxTableViewSectionedAnimatedDataSource<ScoresTableSection>!
    fileprivate let disposeBag = DisposeBag()
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var headerTitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupTableView()
        
        viewModel.getScores()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

fileprivate extension ScoresViewController {
    func setupTableView() {
        let tableView: UITableView = self.tableView
        
        dataSource = RxTableViewSectionedAnimatedDataSource<ScoresTableSection>(configureCell: { (_, _, _, model) -> UITableViewCell in
            return self.cell(for: model)
        })
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = rowHeight
        
        
        
        viewModel.tableViewSections.asDriver()
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        viewModel.tableViewSections.asObservable().bind { (sections) in
            if sections.count > 0 {
                self.headerTitleLabel.text = sections[0].title
            }
        }.disposed(by: disposeBag)
    }
    
    func cell(for viewModel: MatchViewModel) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.cellIdentifier) as! MatchTableViewCell
        
        cell.populate(with: viewModel)
        
        return cell
    }
}
