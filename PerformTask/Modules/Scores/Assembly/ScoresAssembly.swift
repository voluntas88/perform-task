//
//  ScoresAssembly.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

internal final class ScoresAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(ScoresViewModel.self) { (r) in
            let apiService = r.resolve(ApiServiceProtocol.self)
            return ScoresViewModel(apiService: apiService)
        }
        
        container.storyboardInitCompleted(ScoresViewController.self) { r, viewController in
            let viewModel = r.resolve(ScoresViewModel.self)
            viewController.viewModel = viewModel
        }
    }
}
