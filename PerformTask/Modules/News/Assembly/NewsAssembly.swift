//
//  NewsAssembly.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

internal final class NewsAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(NewsViewModel.self) { (r) in
            let apiService = r.resolve(ApiServiceProtocol.self)
            let viewModel = NewsViewModel(apiService: apiService)
            return viewModel
        }
        
        container.register(NewsRouterProtocol.self) { (r, viewController: NewsViewController) in
            return NewsRouter(rootViewController: viewController)
        }
        
        container.storyboardInitCompleted(NewsViewController.self) { r, viewController in
            let viewModel = r.resolve(NewsViewModel.self)
            let router = r.resolve(NewsRouterProtocol.self, argument: viewController)
            viewController.viewModel = viewModel
            viewController.router = router
        }
    }
}
