//
//  NewsTableSection.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import RxDataSources

internal struct NewsTableSection {
    var title: String
    var items: [NewsItemViewModel]
    
    init(title: String, items: [NewsItemViewModel]) {
        self.title = title
        self.items = items
    }
}

//MARK: - AnimatableSectionModelType
extension NewsTableSection: AnimatableSectionModelType {
    
    typealias Identity = String
    
    var identity: Identity {
        return title
    }
    
    init(original: NewsTableSection, items: [NewsItemViewModel]) {
        self = original
        self.items = items
    }
}
