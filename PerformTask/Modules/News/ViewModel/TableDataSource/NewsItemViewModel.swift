//
//  NewsItemViewModel.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import RxDataSources

internal class NewsItemViewModel {
    var text: String
    var detailText: String?
    var imageUrl: String
    var contentUrl: String
    
    init(newsItem: ChannelItem) {
        self.text = newsItem.title
        self.detailText = newsItem.description
        self.imageUrl = newsItem.imageUrl
        self.contentUrl = newsItem.link
    }
    
    let placeholderImageName = "news_placeholder"
}

//MARK: - Equatable
extension NewsItemViewModel: Equatable {
    static func ==(lhs: NewsItemViewModel, rhs: NewsItemViewModel) -> Bool {
        return lhs.text == rhs.text
    }
}

//MARK: - IdentifiableType
extension NewsItemViewModel: IdentifiableType {
    typealias Identity = String
    
    var identity: String {
        return text
    }
}

extension NewsItemViewModel {
    var cellIdentifier: String {
        return "news_cell"
    }
}
