//
//  NewsViewModel.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

internal final class NewsViewModel {
    fileprivate var apiService: ApiServiceProtocol!
    
    fileprivate(set) var tableViewSections: Variable<[NewsTableSection]> = Variable([])
    fileprivate var channel: Variable<Channel> = Variable(Channel())
    
    fileprivate let disposeBag = DisposeBag()
    
    init(apiService: ApiServiceProtocol?) {
        self.apiService = apiService
        self.bind()
    }
}

internal extension NewsViewModel {
    func bind() {
        channel.asObservable().map({ (channelInner) -> [NewsTableSection] in
            let newsItemsViewModels = channelInner.items.map { NewsItemViewModel(newsItem: $0) }
            let section = NewsTableSection(title: channelInner.title, items: newsItemsViewModels)
            return [section]
        }).bind(to: tableViewSections)
        .disposed(by: disposeBag)
    }
    
    func getNews() {
        apiService.fetchObject(from: .news) { (response: DataResponse<Channel>) in
            switch response.result {
            case .success(let channel):
                self.channel.value = channel
            case .failure(_):
                    break
            }
        }
    }
}
