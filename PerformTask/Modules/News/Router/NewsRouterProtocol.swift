//
//  NewsRouterProtocol.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import UIKit

internal protocol NewsRouterProtocol: class {
    func navigateToContentView(title: String, url: String)
}
