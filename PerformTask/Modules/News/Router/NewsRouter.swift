//
//  NewsRouter.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import UIKit

internal final class NewsRouter: NewsRouterProtocol {
    
    fileprivate var rootViewController: UIViewController?
    
    init(rootViewController: UIViewController) {
        self.rootViewController = rootViewController
    }
    
    func navigateToContentView(title: String, url: String) {
        let vc = NewsContentAssembly.viewController
        vc.setContentLink(urlString: url)
        vc.title = title
        rootViewController?.show(vc, sender: rootViewController)
        
    }
    
    
}
