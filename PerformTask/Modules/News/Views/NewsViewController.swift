//
//  NewsViewController.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import AlamofireImage

internal final class NewsViewController: BaseViewController {

    fileprivate let rowHeight:CGFloat = 66.0

    fileprivate var dataSource: RxTableViewSectionedAnimatedDataSource<NewsTableSection>!
    fileprivate let disposeBag = DisposeBag()
    
    var viewModel: NewsViewModel!
    var router: NewsRouterProtocol!
    
    @IBOutlet fileprivate weak var tableView: UITableView!

    @IBOutlet weak var headerTitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewModel.getNews()
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
fileprivate extension NewsViewController {
    func setupTableView() {
        let tableView: UITableView = self.tableView
        
        dataSource = RxTableViewSectionedAnimatedDataSource<NewsTableSection>(configureCell: { (_, _, _, model) -> UITableViewCell in
            return self.cell(for: model)
        })
        
        tableView.rx.modelSelected(NewsItemViewModel.self)
            .subscribe(onNext: onViewModelSelect)
            .disposed(by: disposeBag)
        
        tableView.rowHeight = rowHeight
        
        viewModel.tableViewSections.asDriver()
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        viewModel.tableViewSections.asObservable().bind { (sections) in
            if sections.count > 0 {
                self.headerTitleLabel.text = sections[0].title
            }
            }.disposed(by: disposeBag)
    }
    
    func cell(for viewModel: NewsItemViewModel) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.cellIdentifier) as! NewsTableViewCell
        
        cell.populate(with: viewModel)
        
        return cell
    }
    
    var onViewModelSelect: ((NewsItemViewModel) -> Void) {
        return { viewModel in
            self.router.navigateToContentView(title: viewModel.text, url: viewModel.contentUrl)
        }
    }
}
