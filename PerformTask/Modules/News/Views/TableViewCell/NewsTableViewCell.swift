//
//  NewsTableViewCell.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import UIKit
import AlamofireImage

class NewsTableViewCell: UITableViewCell {
    @IBOutlet fileprivate weak var iconView: UIImageView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populate(with viewModel:NewsItemViewModel) {
        
        titleLabel.text = viewModel.text
        dateLabel.text = viewModel.detailText
        
        if let url = URL(string: viewModel.imageUrl) {
            iconView?.af_setImage(withURL: url, placeholderImage: UIImage(named: viewModel.placeholderImageName))
        }
        
    }

}
