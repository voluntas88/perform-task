//
//  StandingsViewController.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

class StandingsViewController: BaseViewController {

    var viewModel: StandingsViewModel!
    
    fileprivate let rowHeight:CGFloat = 32.0
    
    fileprivate var dataSource: RxTableViewSectionedAnimatedDataSource<StandingsTableSection>!
    fileprivate let disposeBag = DisposeBag()
    
    @IBOutlet fileprivate var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewModel.getStandings()
        
        setupTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

fileprivate extension StandingsViewController {
    func setupTableView() {
        let tableView: UITableView = self.tableView
        
        dataSource = RxTableViewSectionedAnimatedDataSource<StandingsTableSection>(configureCell: { (_, _, indexPath, model) -> UITableViewCell in
            return self.cell(for: model, indexPath: indexPath)
        })
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = rowHeight
        
        viewModel.tableViewSections.asDriver()
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
    
    func cell(for viewModel: RankingViewModel, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.cellIdentifier) as! RankingTableViewCell
        
        cell.populate(with: viewModel, indexPath: indexPath)
        
        return cell
    }
}
