//
//  RankingTableViewCell.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift

class RankingTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var positionTextLabel: UILabel!
    @IBOutlet fileprivate weak var titleTextLabel: UILabel!
    @IBOutlet fileprivate weak var proTextLabel: UILabel!
    @IBOutlet fileprivate weak var wonTextLabel: UILabel!
    @IBOutlet fileprivate weak var drawTextLabel: UILabel!
    @IBOutlet fileprivate weak var lostTextLabel: UILabel!
    @IBOutlet fileprivate weak var goalDiffrenceTextLabel: UILabel!
    @IBOutlet fileprivate weak var pointsTextLabel: UILabel!
    @IBOutlet fileprivate weak var rankStatusImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func populate(with viewModel:RankingViewModel, indexPath: IndexPath) {
        positionTextLabel.text = viewModel.positionText
        titleTextLabel.text = viewModel.titleText
        proTextLabel.text = viewModel.proText
        wonTextLabel.text = viewModel.wonText
        drawTextLabel.text = viewModel.drawText
        lostTextLabel.text = viewModel.lostText
        goalDiffrenceTextLabel.text = viewModel.goalDifferenceText
        pointsTextLabel.text = viewModel.pointsText
        rankStatusImageView.image = UIImage(named: viewModel.rankImageName)?.withRenderingMode(.alwaysTemplate)
        rankStatusImageView.tintColor = UIColor(viewModel.rankColor)
        
        backgroundColor = UIColor(viewModel.backgroundColor(for: indexPath.row))
    }
}
