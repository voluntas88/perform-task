//
//  StandingsViewModel.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

internal struct StandingsViewModel {
    fileprivate var apiService: ApiServiceProtocol!
    
    fileprivate(set) var tableViewSections: Variable<[StandingsTableSection]> = Variable([])
    fileprivate(set) var standings: Variable<Standing> = Variable(Standing())
    
    fileprivate let disposeBag = DisposeBag()
    
    init(apiService: ApiServiceProtocol?) {
        self.apiService = apiService
        self.bind()
    }
}

internal extension StandingsViewModel {
    func bind() {
        standings.asObservable().map({ (standingsInner) -> [StandingsTableSection] in
            let itemViewModels = standingsInner.rankings.sorted { $0.rank < $1.rank }.map { RankingViewModel(ranking: $0) }
            let section = StandingsTableSection(title: standingsInner.title, items: itemViewModels)
            return [section]
        }).bind(to: tableViewSections)
            .disposed(by: disposeBag)
    }
    
    func getStandings() {
        apiService.fetchObject(from: .standing) { (response: DataResponse<Standing>) in
            switch response.result {
            case .success(let standings):
                self.standings.value = standings
            case .failure(_):
                break
            }
        }
    }
}
