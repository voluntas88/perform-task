//
//  StandingsItemViewModel.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import RxDataSources

internal class RankingViewModel {
    
    enum RankStatus {
        case same
        case up
        case down
        
        init(rankDelta: Int) {
            if rankDelta > 0 {
                self = .up
            } else if rankDelta < 0 {
                self = .down
            } else {
                self = .same
            }
        }
    }
    
    var teamId: String
    var titleText: String
    var positionText: String
    var proText: String
    var wonText: String
    var drawText: String
    var lostText: String
    var goalDifferenceText: String
    var pointsText: String
    
    fileprivate var rankStatus: RankStatus
    
    init(ranking: Ranking) {
        self.teamId = ranking.teamId
        self.titleText = ranking.clubName
        self.positionText = RankingViewModel.rankString(rank: ranking.rank)
        self.proText = "\(ranking.goalsPro)"
        self.wonText = ranking.matchesWon
        self.drawText = ranking.matchesDraw
        self.lostText = ranking.matcheLost
        self.goalDifferenceText = RankingViewModel.goalDifference(goalsPro: ranking.goalsPro, goalsAgainst: ranking.goalsAgainst)
        self.pointsText = ranking.points
        self.rankStatus = RankStatus(rankDelta: ranking.rank - ranking.lastRank)
    }
}


extension RankingViewModel {
    var rankColor: String {
        return rankStatus.color
    }
    
    var rankImageName: String {
        return rankStatus.imageName
    }
}

extension RankingViewModel {
    func backgroundColor(for index: Int) -> String {
        return isOdd(index) ? "#ffffff" : "#efefef"
    }
    
    fileprivate func isOdd(_ index: Int) -> Bool{
        return index % 2 > 0
    }
    
}


fileprivate extension RankingViewModel {
    class func goalDifference(goalsPro: Int, goalsAgainst: Int) -> String {
        return "\(goalsPro - goalsAgainst)"
    }
    
    class func rankString(rank: Int) -> String {
        return rank < 10 ? "0\(rank)" : "\(rank)"
    }
}

//MARK: - Equatable
extension RankingViewModel: Equatable {
    static func ==(lhs: RankingViewModel, rhs: RankingViewModel) -> Bool {
        return lhs.teamId == rhs.teamId
    }
}

//MARK: - IdentifiableType
extension RankingViewModel: IdentifiableType {
    typealias Identity = String
    
    var identity: String {
        return teamId
    }
}

extension RankingViewModel {
    var cellIdentifier: String {
        return "ranking_cell"
    }
}

extension RankingViewModel.RankStatus {
    var color: String {
        switch self {
        case .same:
            return "#7b7c7c"
        case .up:
            return "#00aa00"
        case .down:
            return "#b20000"
        }
    }
    
    var imageName: String {
        switch self {
        case .same:
            return "dash"
        case .up:
            return "up_arrow"
        case .down:
            return "down_arrow"
        }
    }
}
