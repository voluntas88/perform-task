//
//  StandingsTableSection.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 20/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import RxDataSources

internal struct StandingsTableSection {
    var title: String
    var items: [RankingViewModel]
    
    init(title: String, items: [RankingViewModel]) {
        self.title = title
        self.items = items
    }
}

//MARK: - AnimatableSectionModelType
extension StandingsTableSection: AnimatableSectionModelType {
    
    typealias Identity = String
    
    var identity: Identity {
        return title
    }
    
    init(original: StandingsTableSection, items: [RankingViewModel]) {
        self = original
        self.items = items
    }
}
