//
//  StandingsAssembly.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

internal final class StandingsAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(StandingsViewModel.self) { (r) in
            let apiService = r.resolve(ApiServiceProtocol.self)
            return StandingsViewModel(apiService: apiService)
        }
        
        container.storyboardInitCompleted(StandingsViewController.self) { r, viewController in
            let viewModel = r.resolve(StandingsViewModel.self)
            viewController.viewModel = viewModel
        }
    }
}
