//
//  CommonAssembly.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

internal final class CommonAssembly: Assembly {
    func assemble(container: Container) {
        container.register(ApiServiceProtocol.self) { (r) in
            ApiService()
        }
    }
}
