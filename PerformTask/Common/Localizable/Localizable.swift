//
//  Extensions.swift
//  PerformTask
//
//  Created by Vladimir Rudenok on 19/11/2017.
//  Copyright © 2017 Volodymyr Rudenok. All rights reserved.
//

import Foundation

enum Localizable: String {
    case menuItemNews = "menu.item.news"
    case menuItemScores = "menu.item.scores"
    case menuItemStanding = "menu.item.standing"
    case urlNoContentUrl = "news.nocontent"
    var localized: String {
        return NSLocalizedString(rawValue, tableName: "Localizable", comment: rawValue)
    }
}
